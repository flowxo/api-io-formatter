var prompt = require('prompt');
var _ = require('lodash');
var util = require('util');

var ARRAY_LENGTH = 3;

prompt.start();

prompt.get(['types (y/N)', 'data'], function (err, input) {
  if(err) {
    return onErr(err);
  }

  var types = input['types (y/N)'];
  types = types != null ? types.toLowerCase() : '';
  types = types.toLowerCase() === 'y';

  var data = JSON.parse(input.data);
  var response = [];

  for (var key in data) {
    var value = data[key];

    if(_.isArray(value)) {
      value = value[0] || '';
      response.push(buildArrayProperties(key, value, types));
    } else if(value instanceof Object) {
      response.push(buildObjectProperties(key, value, types));
    } else {
      response.push(createEntry(key, value, types));
    }
  }

  response = _.flatten(response);
  response = JSON.stringify(response, null, 2)
    .replace(/\"key\":/g, 'key:')
    .replace(/\"label\":/g, 'label:')
    .replace(/\"type\":/g, 'type:')
    .replace(/\"/g, '\'');

  console.log(response);

  return 0;
});

function buildObjectProperties(key, value, type) {
  var results = [];

  for (var prop in value) {
    var subkey = key + '__'  + prop;

    if (_.isPlainObject(value[prop])) {
      var subresults = buildObjectProperties(subkey, value[prop], type);
      results.push(subresults);
    } else {
      results.push(createEntry(subkey, value, type));
    }
  }

  return _.flatten(results);
}

function buildArrayProperties(key, value, type) {
  var results = [];

  for (var i = 0; i < ARRAY_LENGTH; i++) {
    var propKey = key + '__' + i;

    if(_.isPlainObject(value)) {
      for(var subkey in value) {
        results.push(createEntry(propKey + '__' + subkey, value, type));
      }
    } else {
      results.push(createEntry(propKey, value, type));
    }
  }

  return results;
}

function createEntry(key, value, type) {
  var idxMatch = key.match(/__([0-9])(?=__|$)/);
  var correctedLabelKey = key;

  if(idxMatch) {
    var idx = parseInt(idxMatch[1]);
    correctedLabelKey = key.replace(idxMatch[0], '__' + (idx + 1));
  }

  var result = {
    key: key,
    label: formatLabel(correctedLabelKey.replace(/__/g, '_'))
  };

  if (type) {
    switch (typeof value) {
      case 'boolean':
        result.type = 'boolean';
        result.label += '?';
        break;
      default:
        result.type = value === 'DATE' ? 'datetime' : 'text';
        break;
    }
  }

  return result;
}

function formatLabel(key) {
  var parts = key.split(/(?=[A-Z])/).map(function (part) {
    return part.split('_');
  });

  return _.flatten(parts).map(function (word) {
    if (word === 'id' || word === 'ids' || word === 'Id') {
      return 'ID';
    }

    return word.charAt(0).toUpperCase() + word.slice(1);
  }).join(' ');
}

function onErr(err) {
  console.log(err);
  return 1;
}
