# API I/O Formatter

Takes a JSON string of an API response and formats it to `{ key: '', label: '' }`.  
If `input` is set to `y` it will also try and figure out the `type` for the output.  
  
> To output `type: 'datetime'` the value of the input property must be `'DATE'`:  
> `{ date_type_property: 'DATE' }`
  
## Current functionality

* Deep nested objects
* Arrays (will add 3 of each property)
* Input types: `datetime`, `boolean`, `text`
  
## Notes

You must supply the JSON string as a single line. Use an online formatter such as [freeformatter.com](http://www.freeformatter.com/json-formatter.html#ad-output)

## Examples

### Input

```bash
 $ node index.js
prompt: types (y/N):  y
prompt: data:  { "property_text": "some_name", "property_date": "DATE", "property_bool": false, "nested_property": { "sub_key": "", "sub_key_2": false },  
"collection_property": [{ "name": "", "date":"DATE" }, { "name":"other name", "date": "other date"}], "array_property": [1, 2, 3] }
[
  {
    key: 'property_text',
    label: 'Property Text',
    type: 'text'
  },
  {
    key: 'property_date',
    label: 'Property Date',
    type: 'datetime'
  },
  {
    key: 'property_bool',
    label: 'Property Bool?',
    type: 'boolean'
  },
  {
    key: 'nested_property__sub_key',
    label: 'Nested Property Sub Key',
    type: 'text'
  },
  {
    key: 'nested_property__sub_key_2',
    label: 'Nested Property Sub Key 2',
    type: 'text'
  },
  {
    key: 'collection_property__0__name',
    label: 'Collection Property 1 Name',
    type: 'text'
  },
  {
    key: 'collection_property__0__date',
    label: 'Collection Property 1 Date',
    type: 'text'
  },
  {
    key: 'collection_property__1__name',
    label: 'Collection Property 2 Name',
    type: 'text'
  },
  {
    key: 'collection_property__1__date',
    label: 'Collection Property 2 Date',
    type: 'text'
  },
  {
    key: 'collection_property__2__name',
    label: 'Collection Property 3 Name',
    type: 'text'
  },
  {
    key: 'collection_property__2__date',
    label: 'Collection Property 3 Date',
    type: 'text'
  },
  {
    key: 'array_property__0',
    label: 'Array Property 1',
    type: 'text'
  },
  {
    key: 'array_property__1',
    label: 'Array Property 2',
    type: 'text'
  },
  {
    key: 'array_property__2',
    label: 'Array Property 3',
    type: 'text'
  }
]
```

### Output

```bash
 $ node index.js
prompt: types (y/N):  
prompt: data:  { "property_text": "some_name", "property_date": "DATE", "property_bool": false, "nested_property": { "sub_key": "", "sub_key_2": false },  
"collection_property": [{ "name": "", "date":"DATE" }, { "name":"other name", "date": "other date"}], "array_property": [1, 2, 3] }
[
  {
    key: 'property_text',
    label: 'Property Text'
  },
  {
    key: 'property_date',
    label: 'Property Date'
  },
  {
    key: 'property_bool',
    label: 'Property Bool'
  },
  {
    key: 'nested_property__sub_key',
    label: 'Nested Property Sub Key'
  },
  {
    key: 'nested_property__sub_key_2',
    label: 'Nested Property Sub Key 2'
  },
  {
    key: 'collection_property__0__name',
    label: 'Collection Property 1 Name'
  },
  {
    key: 'collection_property__0__date',
    label: 'Collection Property 1 Date'
  },
  {
    key: 'collection_property__1__name',
    label: 'Collection Property 2 Name'
  },
  {
    key: 'collection_property__1__date',
    label: 'Collection Property 2 Date'
  },
  {
    key: 'collection_property__2__name',
    label: 'Collection Property 3 Name'
  },
  {
    key: 'collection_property__2__date',
    label: 'Collection Property 3 Date'
  },
  {
    key: 'array_property__0',
    label: 'Array Property 1'
  },
  {
    key: 'array_property__1',
    label: 'Array Property 2'
  },
  {
    key: 'array_property__2',
    label: 'Array Property 3'
  }
] 
```
